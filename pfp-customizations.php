<?php
/*
Plugin Name: Profits for Pets Site Customizations
Description: Adds the necessary customizations for the PFP website
Version: 2019.1
Author: Curtiss Grymala
Author URI: http://ten-321.com/
License: GPL2
*/

namespace {
	spl_autoload_register( function ( $class_name ) {
		if ( ! stristr( $class_name, 'PFP\Customizations\\' ) ) {
			return;
		}

		$filename = plugin_dir_path( __FILE__ ) . '/lib/classes/' . strtolower( str_replace( array(
				'\\',
				'_'
			), array( '/', '-' ), $class_name ) ) . '.php';

		if ( ! file_exists( $filename ) ) {
			return;
		}

		include $filename;
	} );
}

namespace PFP\Customizations {
	if ( ! isset( $pfp_cust_obj ) || ! is_a( $pfp_cust_obj, '\PFP\Customizations\Plugin' ) ) {
		$GLOBALS['pfp_cust_obj'] = \PFP\Customizations\Plugin::instance();
	}
}