<?php

namespace {
	if ( ! defined( 'ABSPATH' ) ) {
		die( 'You do not have permission to access this file directly.' );
	}
}

namespace PFP\Customizations {
	if ( ! class_exists( 'Plugin' ) ) {
		class Plugin {
			/**
			 * @var \PFP\Customizations\Plugin $instance holds the single instance of this class
			 * @access private
			 */
			private static $instance;
			/**
			 * @var string $version holds the version number for the plugin
			 * @access public
			 */
			public static $version = '2019.1.1';
			/**
			 * @var string $plugin_path the root path to this plugin
			 * @access public
			 */
			public static $plugin_path = '';
			/**
			 * @var string $plugin_url the root URL to this plugin
			 * @access public
			 */
			public static $plugin_url = '';

			/**
			 * Creates the \PFP\Customizations\Plugin object
			 *
			 * @access private
			 * @since  0.1
			 */
			private function __construct() {
				add_filter( 'um_profile_tabs', array( $this, 'um_profile_tabs' ), 11 );
				add_action( 'um_profile_content_locations_default', array( $this, 'do_profile_location_info' ) );
				add_filter( 'um_get_option_filter__profile_menu_default_tab', function () {
					return 'locations';
				} );

				add_filter( 'acf_osm_marker_icon', array( $this, 'marker_icon' ) );

				add_action( 'wp', array( $this, 'choose_layout' ) );
			}

			/**
			 * Returns the instance of this class.
			 *
			 * @access  public
			 * @return  \PFP\Customizations\Plugin
			 * @since   0.1
			 */
			public static function instance() {
				if ( ! isset( self::$instance ) ) {
					$className      = __CLASS__;
					self::$instance = new $className;
				}

				return self::$instance;
			}

			/**
			 * Custom logging function that can be short-circuited
			 *
			 * @access public
			 * @return void
			 * @since  0.1
			 */
			public static function log( $message ) {
				if ( ! defined( 'WP_DEBUG' ) || false === WP_DEBUG ) {
					return;
				}

				error_log( '[PFP Debug]: ' . $message );
			}

			/**
			 * Set the root path to this plugin
			 *
			 * @access public
			 * @return void
			 * @since  1.0
			 */
			public static function set_plugin_path() {
				self::$plugin_path = plugin_dir_path( dirname( dirname( dirname( dirname( __FILE__ ) ) ) ) );
			}

			/**
			 * Set the root URL to this plugin
			 *
			 * @access public
			 * @return void
			 * @since  1.0
			 */
			public static function set_plugin_url() {
				self::$plugin_url = plugin_dir_url( dirname( dirname( dirname( dirname( __FILE__ ) ) ) ) );
			}

			/**
			 * Returns an absolute path based on the relative path passed
			 *
			 * @param string $path the path relative to the root of this plugin
			 *
			 * @access public
			 * @return string the absolute path
			 * @since  1.0
			 */
			public static function plugin_dir_path( $path = '' ) {
				if ( empty( self::$plugin_path ) ) {
					self::set_plugin_path();
				}

				$rt = self::$plugin_path;

				if ( '/' === substr( $path, - 1 ) ) {
					$rt = untrailingslashit( $rt );
				}

				return $rt . $path;
			}

			/**
			 * Returns an absolute URL based on the relative path passed
			 *
			 * @param string $url the URL relative to the root of this plugin
			 *
			 * @access public
			 * @return string the absolute URL
			 * @since  1.0
			 */
			public static function plugin_dir_url( $url = '' ) {
				if ( empty( self::$plugin_url ) ) {
					self::set_plugin_url();
				}

				$rt = self::$plugin_url;

				if ( '/' === substr( $url, - 1 ) ) {
					$rt = untrailingslashit( $rt );
				}

				return $rt . $url;
			}

			/**
			 * Set the appropriate layout/template for the appropriate page(s)
			 *
			 * @access public
			 * @return void
			 * @since  0.1
			 */
			public function choose_layout() {
				if ( is_page( 'locations' ) ) {
					add_filter( 'the_content', array( $this, 'add_map_to_content' ) );
				}
			}

			/**
			 * Insert a selector/search/map at the bottom of the content
			 *
			 * @param string $content the existing content
			 *
			 * @access public
			 * @return string the updated HTML
			 * @since  0.1
			 */
			public function add_map_to_content( $content = '' ) {
				if ( isset( $_GET['location-state'] ) ) {
					$this->_current_location_state = $_GET['location-state'];
				}

				$content .= $this->_get_state_selector();

				$content .= $this->_get_current_map();

				add_action( 'wp_print_footer_scripts', array( $this, 'do_map_scripts' ), 20 );

				return $content;
			}

			/**
			 * Generate a selector of all states that have addresses associated with them
			 *
			 * @access private
			 * @return string the select element
			 * @since  0.1
			 */
			private function _get_state_selector() {
				global $wpdb;
				$states  = $wpdb->get_col( $wpdb->prepare( "SELECT DISTINCT meta_value FROM {$wpdb->usermeta} WHERE meta_key LIKE %s", 'locations\_%\_street\_address\_0\_state' ) );
				$options = array();
				$choices = $this->_get_state_field_choices();

				foreach ( $states as $state ) {
					if ( array_key_exists( $state, $choices ) ) {
						$options[ $state ] = $choices[ $state ];
					}
				}

				if ( empty( $options ) ) {
					return '';
				}

				$o = '<form><select name="location-state"><option value="">-- Select a State --</option>';
				foreach ( $options as $key => $value ) {
					$s = '';
					if ( isset( $this->_current_location_state ) && $key == $this->_current_location_state ) {
						$s = ' selected="selected"';
					}
					$o .= '<option value="' . $key . '"' . $s . '>' . $value . '</option>';
				}
				$o .= '</select><input type="submit" value="Search"/></form>';

				return $o;
			}

			/**
			 * Get the ACF options array for the state field
			 *
			 * @access private
			 * @return array the list of options for the select field
			 * @since  0.1
			 */
			private function _get_state_field_choices() {
				$fields = get_field_objects( 'user_4' );

				foreach ( $fields['locations']['sub_fields'] as $field ) {
					if ( $field['name'] != 'street_address' && $field['name'] != 'state' ) {
						continue;
					}

					if ( 'street_address' == $field['name'] && array_key_exists( 'sub_fields', $field ) ) {
						foreach ( $field['sub_fields'] as $sub_field ) {
							if ( 'state' == $sub_field['name'] ) {
								return $sub_field['choices'];
							}
						}
					}
				}

				return array();
			}

			/**
			 * Get a map for the currently selected area
			 *
			 * @access private
			 * @return string the HTML/JS for the map
			 * @since  0.1
			 */
			private function _get_current_map() {
				if ( isset( $this->_current_location_state ) ) {
					$state = $this->_current_location_state;
				} else {
					$state = 'USA';
				}

				$details = $this->_get_map_center( $state );

				$markers = $this->_get_all_markers();

				$values = array();

				if ( empty( $markers ) ) {
					return '';
				}

				$values['lat']     = $details['lat'];
				$values['lng']     = $details['lng'];
				$values['zoom']    = $details['zoom'];
				$values['markers'] = $markers;

				return \ACFFieldOpenstreetmap\Field\OpenStreetMap::get_instance()->format_value( $values, 'user_4', array(
					'return_format' => 'leaflet',
					'height'        => 600
				) );
			}

			/**
			 * Retrieve the center lat for the current state or country
			 *
			 * @access private
			 * @return string the center lat value
			 * @since  0.1
			 */
			private function _get_map_center( $where ) {
				$lats = array(
					'USA' => array(
						'lat'  => '38.61687046392973',
						'lng'  => '-99.27246093750001',
						'zoom' => 4,
					),
				);

				$csv_location = self::plugin_dir_path( '/lib/includes/geo-state-list.csv' );

				if ( ( $handle = fopen( $csv_location, 'r' ) ) !== false ) {
					while ( ( $data = fgetcsv( $handle ) ) !== false ) {
						if ( 'name' == $data[0] ) {
							continue;
						}

						$zoom = 6;
						if ( $data[9] > 200000 ) {
							$zoom = 4;
						} else if ( $data[9] > 100000 ) {
							$zoom = 5;
						} else if ( $data[9] > 50000 ) {
							$zoom = 6;
						} else if ( $data[9] > 10000 ) {
							$zoom = 7;
						} else {
							$zoom = 8;
						}

						$lats[ $data[1] ] = array(
							'lat'  => $data[6],
							'lng'  => $data[7],
							'zoom' => $zoom,
						);
					}
					fclose( $handle );
				}

				return $lats[ $where ];
			}

			/**
			 * Retrieve a full list of all map markers that need to be put on the map
			 *
			 * @access private
			 * @return array the list of all markers
			 * @since  0.1
			 */
			private function _get_all_markers() {
				$users = get_users( array(
					'role__in' => array(
						'administrator',
						'um_rescue-group',
						'merchant_partner',
					),
				) );

				$markers = array();

				foreach ( $users as $user ) {
					$tmp = $this->_get_user_markers( $user );
					if ( ! empty( $tmp ) && is_array( $tmp ) && array_key_exists( 'markers', $tmp ) ) {
						$tmp     = $tmp['markers'];
						$markers = array_merge( $markers, $tmp );
					}
				}

				return $markers;
			}

			/**
			 * Retrieve a full list of a specific user's map markers
			 *
			 * @param \WP_User the user being queried
			 *
			 * @access private
			 * @return array the list of markers
			 * @since  0.1
			 */
			private function _get_user_markers( $user ) {
				if ( is_numeric( $user ) ) {
					$user_id = $user;
				} else if ( is_a( $user, '\WP_User' ) ) {
					$user_id = $user->ID;
				} else {
					$user_id = 0;
				}

				$output_map = array();

				if ( have_rows( 'locations', 'user_' . $user_id ) ) : while ( have_rows( 'locations', 'user_' . $user_id ) ) : the_row();
					$address = array( 'label' => get_sub_field( 'location_name' ) );
					if ( have_rows( 'street_address', 'user_' . $user_id ) ) : while ( have_rows( 'street_address', 'user_' . $user_id ) ) : the_row();
						$add     = array(
							'number' => get_sub_field( 'street_number' ),
							'name'   => get_sub_field( 'street_name' ),
							'city'   => get_sub_field( 'city' ),
							'state'  => get_sub_field( 'state' ),
							'zip'    => get_sub_field( 'zip_code' ),
						);
						$address = array_merge( $address, $add );
					endwhile; endif;

					$tmp = get_sub_field( 'map_marker' );
                    $store_url = get_sub_field( 'store_url' );
                                     
                    $default_label = vsprintf( '<strong>%1$s</strong><br/>%2$s %3$s<br/>%4$s, %5$s %6$s', $address );
                    $label         = vsprintf( '<strong>%1$s</strong><br/>%2$s %3$s<br/>%4$s, %5$s %6$s', $address );       

                    if ( !empty( $store_url ) ) {                        
                        $store_url = filter_var($store_url, FILTER_SANITIZE_URL);
                        $friendly_url = str_replace(array('http://','https://'), '', $store_url);
                        $store_url = "<br/><a href='" . $store_url . "' target='_blank'>" . $friendly_url . "</a>";

                        $default_label = $default_label . $store_url;
                        $label = $label . $store_url;
                    }

                    $tmp['markers'][0]['default_label'] = $default_label;
                    $tmp['markers'][0]['label']         = $label;
                    $tmp['markers'][0]['full-address']  = $address;                    

                    $continue = false;
					if ( is_array( $tmp['markers'][0] ) ) {
						if ( array_key_exists( 'lat', $tmp['markers'][0] ) && array_key_exists( 'lng', $tmp['markers'][0] ) ) {
							if ( ! empty( $tmp['markers'][0]['lat'] ) && ! empty( $tmp['markers'][0]['lng'] ) ) {
								$continue = true;
							}
						}
					}

					if ( $continue ) {
						if ( isset( $output_map ) ) {
							$output_map['markers'][] = $tmp['markers'][0];
						} else {
							$output_map = $tmp;
						}
					}

				endwhile; endif;

				return $output_map;
			}

			/**
			 * Update the list of profile tabs shown on the Ultimate Member profile
			 *
			 * @param array $tabs the list of existing registered tabs
			 *
			 * @access public
			 * @return array the updated list of tabs
			 * @since  0.1
			 */
			public function um_profile_tabs( $tabs = array() ) {
				foreach ( array( /*'main', */ 'posts', 'comments' ) as $t ) {
					if ( array_key_exists( $t, $tabs ) ) {
						unset( $tabs[ $t ] );
					}
				}

				$tabs['locations'] = array(
					'name'   => __( 'Location(s)', 'pfp-cust' ),
					'icon'   => 'fas fa-map-pin',
					'custom' => true,
				);

				UM()->options()->options[ 'profile_tab_' . 'locations' ] = true;

				return $tabs;
			}

			/**
			 * Implements the map/location information on the user's main profile page
			 *
			 * @access public
			 * @return void
			 * @since  0.1
			 */
			public function do_profile_location_info() {
				if ( true == UM()->fields()->editing ) {
					$this->do_profile_location_fields();

					return;
				}

				$user_id = um_profile_id();
				echo '<div class="location-information"><h2>Business Locations</h2>';

				$output_map = $this->_get_user_markers( $user_id );

				if ( ! empty( $output_map ) ) {
					foreach ( $output_map['markers'] as $marker ) {
						printf( '<p class="street-address">%s</p>', $marker['label'] );
					}

					echo \ACFFieldOpenstreetmap\Field\OpenStreetMap::get_instance()->format_value( $output_map, 'user_' . $user_id, array(
						'return_format' => 'leaflet',
						'height'        => 400
					) );
				}

				echo '</div>';
				add_action( 'wp_print_footer_scripts', array( $this, 'do_map_scripts' ), 20 );
			}

			/**
			 * Update the map's marker icons
			 *
			 * @access public
			 * @return array the leaflet icon object options for the appropriate icon
			 * @since  0.1
			 */
			public function marker_icon( $icon ) {
				return array(
					'iconUrl'       => self::plugin_dir_url( '/lib/images/pfp-map-marker-24.png' ),
					'iconRetinaUrl' => self::plugin_dir_url( '/lib/images/pfp-map-marker-48.png' ),
					'iconSize'      => array( 37, 24 ),
					'iconAnchor'    => array( 11, 24 ),
					'popupAnchor'   => array( 2, - 24 ),
				);
			}

			/**
			 * Output some custom JS for the map
			 *
			 * @access public
			 * @return void
			 * @since  0.1
			 */
			public function do_map_scripts() {
				echo '<script type="text/javascript" src="' . self::plugin_dir_url( '/lib/includes/us-states.json' ) . '"></script>';
				echo '<script type="text/javascript">';
				echo '
				console.log( L );
				jQuery(document).on("acf-osm-map-init", function (e, map) {
				    L.geoJSON(statesData, {
				        style: function (feature) {
				            var tmpStyle = {
				                color: "#000",
				                weight: "2",
				                opacity: "0.7",
				                fill: false
				            };
				            var currentState = jQuery(\'select[name="location-state"] option:selected\').text();
				            if (feature.properties.name == currentState) {
				                tmpStyle.fill = true;
				                tmpStyle.fillColor = "rgb(220,33,60)";
				                tmpStyle.fillOpacity = "0.3";
				            }
				            return tmpStyle;
				        }
				    }).addTo(map);
				});';
				echo '</script>';
			}
		}
	}
}