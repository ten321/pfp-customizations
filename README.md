# Profits for Pets Customizations
* Contributors: cgrymala
* Requires at least: 5.2
* Tested up to: 5.2
* Requires PHP: 7
* Stable tag: 2019.1
* License: GPLv2 or Later
* License URI: https://www.gnu.org/licenses/gpl-2.0.html

Implements any customizations that are necessary for the Profits for Pets website

## Description
Implements any customizations that are necessary for the Profits for Pets website. Implements the locaiton/address information for a user on their Ultimate Member user profile page.
